//////////////////////////////////////
//  Colin Hussey - CSE002 - Lab 04  //
//       Prof. Brian Chen           //
//////////////////////////////////////


import java.util.Random;

//Creating the public class and first method
public class PokerHandCheck{
    public static void main(String[]args) {
   
    //Declaring strings
     String cardSuit = null;
      String cardSuit1 = null;
      String cardSuit2 = null;
      String cardSuit3 = null;
      String cardSuit4 = null;
      String cardSuit5 = null;
     String cardIdentity = null;
      String cardIdentity1 = null;
      String cardIdentity2 = null;
      String cardIdentity3 = null;
      String cardIdentity4 = null;
      String cardIdentity5 = null;
   
    ///////////////////////////CARD GEN BLOCK////////////////////////////////
      
    // Obtain a number between 0 and 51
    Random rand1 = new Random();
    int n1 = rand1.nextInt(52);
    // Add 1 to it
    n1 += 1;
    
    int num1 = (n1 % 13);
    
    //Making String for suit using if/elseif statements
    if (n1 <= 13){
       cardSuit1 = "Diamonds";
    } else if (n1 < 26) {
       cardSuit1 = "Clubs";
    } else if (n1 <= 39) {
       cardSuit1 = "Hearts";
    } else if (n1 <= 52) {
       cardSuit1 = "Spades";
  }
     //Making string variable for card identity
    if (1 < (n1 % 13) && (n1 % 13) < 11){
       cardIdentity1 = Integer.toString(n1 % 13);
    } else if ((n1 % 13) == 1){
       cardIdentity1 = "Ace";
    } else if (n1 % 13 == 11){
       cardIdentity1 = "Jack";
    } else if (n1 % 13 == 12){
       cardIdentity1 = "Queen";
    } else if (n1 % 13 == 0){
       cardIdentity1 = "King";
    } 
   
    ///////////////////////////CARD GEN BLOCK////////////////////////////////
    Random rand2 = new Random();
    int n2 = rand2.nextInt(52);
    n2 += 1;
    int num2 = (n2 % 13);
    if (n2 <= 13){
       cardSuit2 = "Diamonds";
    } else if (n2 < 26) {
       cardSuit2 = "Clubs";
    } else if (n2 <= 39) {
       cardSuit2 = "Hearts";
    } else if (n2 <= 52) {
       cardSuit2 = "Spades";
  }
    if (1 < (n2 % 13) && (n2 % 13) < 11){
       cardIdentity2 = Integer.toString(n2 % 13);
    } else if ((n2 % 13) == 1){
       cardIdentity2 = "Ace";
    } else if (n2 % 13 == 11){
       cardIdentity2 = "Jack";
    } else if (n2 % 13 == 12){
       cardIdentity2 = "Queen";
    } else if (n2 % 13 == 0){
       cardIdentity2 = "King";
    }
    ///////////////////////////CARD GEN BLOCK////////////////////////////////
      Random rand3 = new Random();
    int n3 = rand3.nextInt(52);
    n3 += 1;
    int num3 = (n3 % 13);
    if (n3 <= 13){
       cardSuit3 = "Diamonds";
    } else if (n3 < 26) {
       cardSuit3 = "Clubs";
    } else if (n3 <= 39) {
       cardSuit3 = "Hearts";
    } else if (n3 <= 52) {
       cardSuit3 = "Spades";
  }
    if (1 < (n3 % 13) && (n3 % 13) < 11){
       cardIdentity3 = Integer.toString(n3 % 13);
    } else if ((n3 % 13) == 1){
       cardIdentity3 = "Ace";
    } else if (n3 % 13 == 11){
       cardIdentity3 = "Jack";
    } else if (n3 % 13 == 12){
       cardIdentity3 = "Queen";
    } else if (n3 % 13 == 0){
       cardIdentity3 = "King";
    }
    ///////////////////////////CARD GEN BLOCK////////////////////////////////
      Random rand4 = new Random();
    int n4 = rand4.nextInt(52);
    n4 += 1;
    int num4 = (n4 % 13);
    if (n4 <= 13){
       cardSuit4 = "Diamonds";
    } else if (n4 < 26) {
       cardSuit4 = "Clubs";
    } else if (n4 <= 39) {
       cardSuit4 = "Hearts";
    } else if (n4 <= 52) {
       cardSuit4 = "Spades";
  }
    if (1 < (n4 % 13) && (n4 % 13) < 11){
       cardIdentity4 = Integer.toString(n4 % 13);
    } else if ((n4 % 13) == 1){
       cardIdentity4 = "Ace";
    } else if (n4 % 13 == 11){
       cardIdentity4 = "Jack";
    } else if (n4 % 13 == 12){
       cardIdentity4 = "Queen";
    } else if (n4 % 13 == 0){
       cardIdentity4 = "King";
    }
    ///////////////////////////CARD GEN BLOCK////////////////////////////////
      Random rand5 = new Random();
      int n5 = rand5.nextInt(52);
      n5 += 1;
      int num5 = (n5 % 13);
    if (n5 <= 13){
       cardSuit5 = "Diamonds";
    } else if (n5 < 26) {
       cardSuit5 = "Clubs";
    } else if (n5 <= 39) {
       cardSuit5 = "Hearts";
    } else if (n5 <= 52) {
       cardSuit5 = "Spades";
  }
    if (1 < (n5 % 13) && (n5 % 13) < 11){
       cardIdentity5 = Integer.toString(n5 % 13);
    } else if ((n5 % 13) == 1){
       cardIdentity5 = "Ace";
    } else if (n5 % 13 == 11){
       cardIdentity5 = "Jack";
    } else if (n5 % 13 == 12){
       cardIdentity5 = "Queen";
    } else if (n5 % 13 == 0){
       cardIdentity5 = "King";
    }
    ///////////////////////////CARD GEN BLOCK////////////////////////////////
    
     System.out.print("Your random cards were: \n"); 
     System.out.print("    the " + cardIdentity1 + " of " + cardSuit1 + "\n");
     System.out.print("    the " + cardIdentity2 + " of " + cardSuit2 + "\n");
     System.out.print("    the " + cardIdentity3 + " of " + cardSuit3 + "\n");
     System.out.print("    the " + cardIdentity4 + " of " + cardSuit4 + "\n");
     System.out.print("    the " + cardIdentity5 + " of " + cardSuit5 + "\n");

    //Checking if any 3 of the 5 cards are the same  
     if  ((n1 == n2 && n1 == n3) ||
         (n1 == n2 && n1 == n4) ||
         (n1 == n2 && n1 == n5) ||
         (n1 == n3 && n1 == n4) ||
         (n1 == n3 && n1 == n5) ||
         (n1 == n4 && n1 == n5) ||
         (n2 == n3 && n2 == n4) ||
         (n2 == n3 && n2 == n5) ||
         (n2 == n4 && n2 == n5) ||
         (n3 == n4 && n3 == n5) ){
       System.out.print("You have a three of a kind! \n");
     //Checking if any 2 of the 5 cards are the same
     } else if (
         (n1 == n2) ||
         (n1 == n3) ||
         (n1 == n4) ||
         (n1 == n5) ||
         (n2 == n3) ||
         (n2 == n4) ||
         (n2 == n5) ||
         (n3 == n4) ||
         (n3 == n5) ||
         (n4 == n5) 
         ){
       System.out.print("You have a pair! \n");
     //Without using loops, the following is the best way I could think
     //to check if there were two pairs in the 5 cards
     } else if(
         ((n1 == n2) && (n1 == n3)) ||
         ((n1 == n2) && (n1 == n4)) ||
         ((n1 == n2) && (n1 == n5)) ||
         ((n1 == n2) && (n2 == n3)) ||
         ((n1 == n2) && (n2 == n4)) ||
         ((n1 == n2) && (n2 == n5)) ||
         ((n1 == n2) && (n3 == n4)) ||
         ((n1 == n2) && (n3 == n5)) ||
         ((n1 == n2) && (n4 == n5)) ||
       
         ((n1 == n3) && (n1 == n4)) ||
         ((n1 == n3) && (n1 == n5)) ||
         ((n1 == n3) && (n2 == n3)) ||
         ((n1 == n3) && (n2 == n4)) ||
         ((n1 == n3) && (n2 == n5)) ||
         ((n1 == n3) && (n3 == n4)) ||
         ((n1 == n3) && (n3 == n5)) ||
         ((n1 == n3) && (n4 == n5)) ||
       
         ((n1 == n4) && (n1 == n5)) ||
         ((n1 == n4) && (n2 == n3)) ||
         ((n1 == n4) && (n2 == n4)) ||
         ((n1 == n4) && (n2 == n5)) ||
         ((n1 == n4) && (n3 == n4)) ||
         ((n1 == n4) && (n3 == n5)) ||
         ((n1 == n4) && (n4 == n5)) ||
       
         ((n1 == n5) && (n2 == n3)) ||
         ((n1 == n5) && (n2 == n4)) ||
         ((n1 == n5) && (n2 == n5)) ||
         ((n1 == n5) && (n3 == n4)) ||
         ((n1 == n5) && (n3 == n5)) ||
         ((n1 == n5) && (n4 == n5)) ||
       
         ((n2 == n3) && (n2 == n4)) ||
         ((n2 == n3) && (n2 == n5)) ||
         ((n2 == n3) && (n3 == n4)) ||
         ((n2 == n3) && (n3 == n5)) ||
         ((n2 == n3) && (n4 == n5)) ||
       
         ((n2 == n4) && (n2 == n5)) ||
         ((n2 == n4) && (n3 == n4)) ||
         ((n2 == n4) && (n3 == n5)) ||
         ((n2 == n4) && (n4 == n5)) ||
       
         ((n2 == n5) && (n3 == n4)) ||
         ((n2 == n5) && (n3 == n5)) ||
         ((n2 == n5) && (n4 == n5)) ||
        
         ((n3 == n4) && (n3 == n5)) ||
         ((n3 == n4) && (n4 == n5)) ||
       
         ((n3 == n5) && (n4 == n5)) 
     ){
       System.out.print("You have a two pair! \n");
     } else{
       System.out.print("You have a high card hand! \n");
     }


  }
}