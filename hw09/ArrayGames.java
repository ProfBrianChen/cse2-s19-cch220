/////////////////////////////////////     
//  Colin Hussey - CSE002 - hw 09  //
//        Prof. Brian Chen         //
/////////////////////////////////////

import java.util.Random;
import java.util.Scanner;


public class ArrayGames{
  
public static void main ( String[] args ){
 
 Scanner myScanner = new Scanner ( System.in );

 // asking for function type (variable 'test' -> 1=input, 0=shorten)
  int test = 0;
  boolean ok = false;
  while( ok == false ){
        System.out.print( "Input(1) or Shorten(0)? input the number in (): " );
        boolean a = myScanner.hasNextInt();
        if( a == true ) {
            test = myScanner.nextInt();
          if( test == 1 || test == 0 ){
            ok = true;
          } else {
            System.out.println( "Did not enter 0 or 1, try again." );
          }
        } else if(a != true) {
            System.out.println( "Input must be a number; try again." );
            test = 0;
            String junkWord = myScanner.next();
        }
     }  
  
 // checking which function to use
if ( test == 1){
  
  //generating 2 arrays
  int[] array1 = generate();
  int[] array2 = generate();
  
  //printing them out with dividers and labels
  System.out.println("Array1");
  print(array1);
  System.out.println("-----------------------");
  System.out.println("Array2");
  print(array2);
  System.out.println("-----------------------");
  
  //using insert() to combine the two arrays
  int[] array3 = insert( array1, array2 );
  
  //printing the new array
  System.out.println("-----------------------");
  System.out.println("Array3");
  print(array3);
  System.out.println("-----------------------");
  
  //if 'shorten' function was chosen
} else if ( test == 0){
  
  //generating and printing the new array
  int[] array1 = generate();
  System.out.println("Array1");
  print(array1);
  System.out.println("-----------------------");
  
  //asking user which value to remove
  int remove = 0;
  while( remove == 0 ){
        System.out.print( "Input the index of the value to remove: " );
        boolean a = myScanner.hasNextInt();
        if( a == true ){
            remove = myScanner.nextInt();
          if (remove < 0){
            remove = 0;
            System.out.println( "Input must be a POSITIVE number; try again." );
          } else if( remove > array1.length){
            remove = 0;
            System.out.println( "Input must be within the array; try again." );
         } else if(a != true) {
            System.out.println( "Input must be a number; try again." );
            remove = 0;
            String junkWord = myScanner.next();
      }
    }
  }
  
  //removing that value and printing the resulting array
  int[] array2 = shorten( array1, remove );
  System.out.println("-----------------------");
  System.out.println("Array2:");
  print(array2);
  System.out.println("-----------------------");
  
} 
}

public static int[] generate(){
 
  int size;
  
  //generating random size from 10-20
  do{
    double tempVal = Math.random();
    size = (int) (tempVal * 100);
  }while( size <= 10 || size >= 20 );
  
  System.out.println("size = "+ size);
  
  //initializing new array of that size
  int[] newArray = new int[size];
  
  //initializing start value for array inputs
   double tempVal2 = Math.random();
   int inVal = (int) (tempVal2 * 100);
  
  //filling the array with sequential inputs
  for( int i = 0; i < newArray.length; i++ ){
    newArray[i] = inVal;
    inVal++;
  }
  
  return newArray;
  
}
  
public static void print(int[] array){
  for( int i = 0; i < array.length; i++){
    System.out.println("index " + i + ": " + array[i] );
  }
}  
  
public static int[] insert(int[] ArrayA, int[] ArrayB){
  //initializing new array with length equal to the combined lengths of inputs
  int [] ArrayC = new int[ArrayA.length + ArrayB.length];
  int index;
  
  //generating a random index at which the second array will be inputted
  do{
    double tempVal = Math.random();
    index = (int) (tempVal * 100);
  }while( index > ArrayA.length );
  System.out.println("insertion point: " + index);
  
  //for loop to fill the new array based on the insertion point
  for( int k = 0; k < (ArrayA.length + ArrayB.length); k++ ){
    if( k < index ){
      ArrayC[k] = ArrayA[k];
    } else if( k >= index && k < (index + ArrayB.length) ){
      ArrayC[k] = ArrayB[k - index];
    } else if( k >= (ArrayB.length + index) && k < (ArrayB.length + ArrayA.length) ){
      ArrayC[k] = ArrayA[k - ArrayB.length];
    }
  }
 return ArrayC; 
}
  
public static int[] shorten(int[] Array, int remove){
  //checking if inputted value is within the viable range
  if( remove > Array.length){
    return Array;
  //if it is
  } else {
    
    //intitializing new array with legnth = original length - 1
    int[] newArray = new int[Array.length - 1];
   
    //filling that new array with all values EXCEPT the removed value
    for( int i = 0; i < newArray.length; i++){
      if( i < remove){
        newArray[i] = Array[i];
      } else if ( i >= remove){
        newArray[i] = Array[i+1];
      }
    }
    return newArray;
  }
  
}
  
  
  
}