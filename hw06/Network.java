/////////////////////////////////////     
//  Colin Hussey - CSE002 - HW 06  //
//        Prof. Brian Chen         //
/////////////////////////////////////

//Importing the scanner
import java.util.Scanner;
   
public class Network {
    	// main method required for every Java program
   	public static void main(String[] args) {
      
      //Initialize scanner
    Scanner myScanner = new Scanner ( System.in );
      
      //Initialize variables      
     int height = 0;
     int width = 0;
     int size = 0;
     int length = 0;
     String junkWord = new String("empty");

      
      while( height == 0 ){
        System.out.print( "Input your desired window height: " );
        boolean a = myScanner.hasNextInt();
        if( a == true ){
            height = myScanner.nextInt();
          if (height < 0){
            height = 0;
            System.out.println( "Input must be a POSITIVE number; try again." );
          }
        } else if(a != true) {
            System.out.println( "Input must be a number; try again." );
            height = 0;
            junkWord = myScanner.next();
          
      }
     }
      
      while( width == 0 ){
        System.out.print( "Input your desired window width: " );
        boolean a = myScanner.hasNextInt();
        if( a == true ){
            width = myScanner.nextInt();
          if (width < 0){
            width = 0;
            System.out.println( "Input must be a POSITIVE number; try again." );
          }
        } else if(a != true) {
            System.out.println( "Input must be a number; try again." );
            width = 0;
            junkWord = myScanner.next();
        
      }
     }
      
      while( size == 0 ){
        System.out.print( "Input your desired square size: " );
        boolean a = myScanner.hasNextInt();
        if( a == true ){
            size = myScanner.nextInt();
          if (size < 0){
            size = 0;
            System.out.println( "Input must be a POSITIVE number; try again." );
          }
        } else if(a != true) {
            System.out.println( "Input must be a number; try again." );
            size = 0;
            junkWord = myScanner.next();
      }
     }
      
      while( length == 0 ){
        System.out.print( "Input your desired edge length: " );
        boolean a = myScanner.hasNextInt();
        if( a == true ){
            length = myScanner.nextInt();
          if (length < 0){
            length = 0;
            System.out.println( "Input must be a POSITIVE number; try again." );
          }
        } else if(a != true) {
            System.out.println( "Input must be a number; try again." );
            length = 0;
            junkWord = myScanner.next();
      }
     }

     int windowX = 1; //window x position
     int squareX = 1; //in-square x position
     int spaceX = 1; //in-between square x position
     int windowY = 1; //window y position
     int squareY = 1; //in-square y position
     int spaceY = 1; //in-between square y position
     boolean even = false; //boolean variable storing even/odd state of square size
      
    // determine if squares are evenly or oddly sized
    if ( size % 2 == 0 ){
      even = true;
    }
    
    
    for( windowY = 1; windowY <= height; ){
        for( squareY = 1; squareY <= size; squareY++){ //"For the first square(vertical sense)"
            if( windowY <= height ){ //"while still in the constraints of number of lines"
                if( (squareY % size) == 1 || (squareY % size) == 0 ){ //"if its the first or last row of the square(vertical sense)"
                    for( windowX = 1; windowX <= width; ){
                        for( squareX = 1; squareX <= size; squareX++){ //"for the first square(horizontal sense)"
                            if( (squareX % size) == 1 || (squareX % size) == 0 && windowX <= width ){ //"if its the first/last column of the square"
                                System.out.print("#"); 
                                windowX++;
                            } 
                            else if( windowX <= width ){ //"if its not the first/last column"
                                System.out.print("-");
                                windowX++;                              
                            }
                        }
                        for( spaceX = 1; spaceX <= length; spaceX++ ){//"for the first space..."
                            if( windowX <= width ){
                                System.out.print(" ");
                                windowX++;
                            }
                        }
                    }   
                }
                else if( (even == true) && (((size/2)+0.5)-squareY) < 1 && (((size/2)+0.5)-squareY) > -1){ //if its the middle of the square (even)
                    for( windowX = 1; windowX <= width; ){
                        for( squareX = 1; squareX <= size; squareX++){
                            if( (squareX % size) == 1 || (squareX % size) == 0 && windowX <= width ){
                                System.out.print("|");
                                windowX++;
                            } 
                            else if( windowX <= width){
                                System.out.print(" ");
                                windowX++;                              
                            }
                        }
                        for( spaceX = 1; spaceX <= length; spaceX++ ){
                            if( windowX <= width ){
                                System.out.print("-");
                                windowX++;
                            }
                        }
                    }   
                }
                else if( (even == false) && (((size/2)+0.5)-squareY) < 0 && (((size/2)+0.5)-squareY) > -1){ //if its the middle of the square (odd)
                    for( windowX = 1; windowX <= width; ){
                        for( squareX = 1; squareX <= size; squareX++){
                            if( (squareX % size) == 1 || (squareX % size) == 0 && windowX <= width ){
                                System.out.print("|");
                                windowX++;
                            } 
                            else if( windowX <= width){
                                System.out.print(" ");
                                windowX++;                              
                            }
                        }
                        for( spaceX = 1; spaceX <= length; spaceX++ ){
                            if( windowX <= width ){
                                System.out.print("-");
                                windowX++;
                            }
                        }
                    } 
                }
                else{ //"if not the first/last row of a square (vertical sense) or the middle"
                    for( windowX = 1; windowX <= width; ){
                        for( squareX = 1; squareX <= size; squareX++){
                            if( (squareX % size) == 1 || (squareX % size) == 0 && windowX <= width ){
                                System.out.print("|");
                                windowX++;
                            } 
                            else if( windowX <= width){
                                System.out.print(" ");
                                windowX++;                              
                            }
                        }
                        for( spaceX = 1; spaceX <= length; spaceX++ ){
                            if( windowX <= width ){
                                System.out.print(" ");
                                windowX++;
                            }
                        }
                    }   
                }
             System.out.println("");
             windowY++;
            }
        }
        for( spaceY = 1; spaceY <= length; spaceY++ ){//"for the first space...(vertical)"
            for( windowX = 1; windowX <= width; ){
                    for( squareX = 1; squareX <= size; squareX++){ //"for the first square(horizontal sense)"
                            if( (squareX % size) == 1 || (squareX % size) == 0 && windowX <= width && windowY <= height ){ //"if its the first/last column of the square"
                                System.out.print(" "); 
                                windowX++;
                            } 
                            else if( (even == true) && (((size/2)+0.5)-squareX) < 1 && (((size/2)+0.5)-squareX) > -1 && windowX <= width && windowY <= height){
                                System.out.print("|");
                                windowX++;
                            }
                            else if( (even == false) && (((size/2)+0.5)-squareX) < 0 && (((size/2)+0.5)-squareX) > -1 && windowX <= width && windowY <= height ){
                                System.out.print("|");
                                windowX++;
                            }
                            else if( windowX <= width && windowY <= height ){ //"if its not the first/last column"
                                System.out.print(" ");
                                windowX++;                              
                            }
                    }
                    for( spaceX = 1; spaceX <= length; spaceX++ ){//"for the first space..."
                        if( windowX <= width ){
                            System.out.print(" ");
                            windowX++;
                        }
                    }
            }
            System.out.println("");
            windowY++;   
        }
      }
   }
}
        






