/////////////////////////////////////     
//  Colin Hussey - CSE002 - lab 07 //
//        Prof. Brian Chen         //
/////////////////////////////////////

import java.util.Random;
import java.util.Scanner;


public class Sentences {
  // main method required for every Java program
 
    public static String randAdjective(){
        Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt(10);
        String randAdj = "";
        switch ( randomInt ){
            case 0: randAdj = "quick";
                break;
            case 1: randAdj = "slow";
                break;
            case 2: randAdj = "hot";
                break;
            case 3: randAdj = "cold";
                break;
            case 4: randAdj = "red";
                break;
            case 5: randAdj = "blue";
                break;
            case 6: randAdj = "smart";
                break;
            case 7: randAdj = "weird";
                break;
            case 8: randAdj = "old";
                break;
            case 9: randAdj = "young";
                break;        
        }
      return randAdj;
    }

    public static String randNounSubject(){
       Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt(10);
        String randNounS = "";
        switch ( randomInt ){
            case 0: randNounS = "man";
                break;
            case 1: randNounS = "boy";
                break;
            case 2: randNounS = "woman";
                break;
            case 3: randNounS = "girl";
                break;
            case 4: randNounS = "fox";
                break;
            case 5: randNounS = "squirrell";
                break;
            case 6: randNounS = "mouse";
                break;
            case 7: randNounS = "teacher";
                break;
            case 8: randNounS = "TA";
                break;
            case 9: randNounS = "student";
                break;        
        }    
      return randNounS;
    }

    public static String randNounObject(){
       Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt(10);
        String randNounO = "";
        switch ( randomInt ){
            case 0: randNounO = "man";
                break;
            case 1: randNounO = "boy";
                break;
            case 2: randNounO = "woman";
                break;
            case 3: randNounO = "girl";
                break;
            case 4: randNounO = "fox";
                break;
            case 5: randNounO = "squirrell";
                break;
            case 6: randNounO = "mouse";
                break;
            case 7: randNounO = "teacher";
                break;
            case 8: randNounO = "TA";
                break;
            case 9: randNounO = "student";
                break;        
        }  
      return randNounO;
    }

    public static String randNounObjectPlur(){
        Random randomGenerator = new Random();
         int randomInt = randomGenerator.nextInt(10);
         String randNounOP = "";
         switch ( randomInt ){
             case 0: randNounOP = "men";
                 break;
             case 1: randNounOP = "boys";
                 break;
             case 2: randNounOP = "women";
                 break;
             case 3: randNounOP = "girls";
                 break;
             case 4: randNounOP = "foxes";
                 break;
             case 5: randNounOP = "squirrells";
                 break;
             case 6: randNounOP = "mice";
                 break;
             case 7: randNounOP = "teachers";
                 break;
             case 8: randNounOP = "TA's";
                 break;
             case 9: randNounOP = "students";
                 break;        
         }  
       return randNounOP;
    }
    
    public static String randVerb(){
       Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt(10);
        String randVerb = "";
        switch ( randomInt ){
            case 0: randVerb = "passed";
                break;
            case 1: randVerb = "taught";
                break;
            case 2: randVerb = "jumped";
                break;
            case 3: randVerb = "lapped";
                break;
            case 4: randVerb = "smacked";
                break;
            case 5: randVerb = "whacked";
                break;
            case 6: randVerb = "bonked";
                break;
            case 7: randVerb = "helped";
                break;
            case 8: randVerb = "confused";
                break;
            case 9: randVerb = "pushed";
                break;        
        }    
      return randVerb;
    }

    public static String randNounS = " ";

    public static String randSentence1(){
        Random randomGenerator = new Random();
        String sentence = "The ";
        int i = 1;
        randNounS = randNounSubject();
        while( i < 8 ){
            if( i == 1 || i == 2 || i == 6 ){
                String newString = randAdjective();
                sentence += newString;
                sentence += " ";
                i++;
            }
            else if( i == 3 ){
                sentence += randNounS;
                sentence += " ";
                i++;
            }
            else if( i == 4 ){
                String newString = randVerb();
                sentence += newString;
                sentence += " ";
                i++;
            }
            else if( i == 5 ){
                sentence += "the ";
                i++;
            }
            else if( i == 7 ){
                String newString = randNounObject();
                sentence += newString;
                sentence += ".";
                i++;
            }
        }
     return sentence;

    }

    public static String randSentence2(String randNounS){
        Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt(10);
        int i = 1;
        String sentence = "";
        while( i < 7 ){
            if( i == 1 ){
                if (randomInt < 5){
                    sentence += "This ";
                    sentence += randNounS;
                    sentence += " ";
                    i++;
                }
                else {
                    sentence += "It ";
                    i++;
                }
            }
            else if( i == 2 ){
                sentence += "was ";
                i++;
            }
            else if( i == 3 || i == 5){
                String newString = randAdjective();
                sentence += newString;
                sentence += " ";
                i++;
            }
            else if( i == 4 ){
                sentence += "to ";
                i++;
            }
            else if( i == 6 ){
                String newString = randNounObjectPlur();
                sentence += newString;
                sentence += ".";
                i++;
            }
        }
    return sentence;
    }

    public static String randSentence3(String randNounS){
        String sentence = "";
        int i = 1;
        while( i < 6 ){
            if( i == 1 ){
                sentence += "This ";
                sentence += " ";
                i++;
            }
            else if( i == 2 ){
                sentence += randNounS;
                sentence += " ";
                i++;
            }
            else if( i == 3 ){
                String newString = randVerb();
                sentence += newString;
                i++;
            }
            else if( i == 4 ){
                String newString = randNounObjectPlur();
                sentence += "their ";
                i++;
            }
            else if( i == 5 ){
                String newString = randNounObject();
                sentence += newString;
                sentence += "!";
                i++;
            }
        }
     return sentence;

    }

    public static void main(String[] args) {
      
      Random randomGenerator = new Random();
      Scanner myScanner = new Scanner ( System.in );


      String sentence1 = randSentence1();
      System.out.println(sentence1);

      boolean newSent = false;
      int testNum = 0;
      int ok = 0;
      String junkWord = "";

    while( ok == 0 ){
        System.out.print( "Would you like a different sentence? (1 for 'yes', 0 for 'no')");
        testNum = myScanner.nextInt();
        if ( testNum == 0 ){
            newSent = false;
            ok = 1;
        }
        else if ( testNum == 1 ){
            newSent = true;
            sentence1 = randSentence1();
            System.out.println(sentence1);
            ok = 0;
        }
    } 

      String sentence2 = randSentence2(randNounS);
      System.out.println(sentence2);

      String sentence3 = randSentence3(randNounS);
      System.out.println(sentence3);

     


  }

}



  
