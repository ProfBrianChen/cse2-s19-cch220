/////////////////////////////////////     
//  Colin Hussey - CSE002 - lab 10 //
//        Prof. Brian Chen         //
/////////////////////////////////////

import java.util.Random;
import java.util.Scanner;


public class TwoDArrays{
public static void main ( String[] args ){
  
 double tempVal;
 int widthAB, heightAB, widthC, heightC;
 //generating random width and height for A and B
 widthAB = (int) (Math.random()*((10-5)+1))+5;
 heightAB = (int) (Math.random()*((10-5)+1))+5;
 //generating random width and height for C
 widthC = (int) (Math.random()*((10-5)+1))+5;
 heightC = (int) (Math.random()*((10-5)+1))+5;

 //creating A and B matrices (equally sized)
 int[][] A = increasingMatrix(widthAB, heightAB, true);
 System.out.print("Matrix A: ");
 printMatrix(A, true);
 int[][] B = increasingMatrix(widthAB, heightAB, false);
 System.out.print("Matrix B: ");
 printMatrix(B, false);
 
 //creating C matrix (random size)
 int[][] C = increasingMatrix(widthC, heightC, false);
 System.out.print("Matrix C: ");
 printMatrix(C, false);
 
 //adding matrix A and B
 int[][] addedAB = addMatrix(A, true, B, false);
 System.out.print("Matrix A + B: ");
 printMatrix(addedAB, true);
  
 //adding matrix A and C
 System.out.println("Attempting to add A + C ...");
 int[][] addedAC = addMatrix(A, true, C, false);
 if(addedAC != null){
 System.out.print("Matrix A + C: ");
 printMatrix(addedAC, true);
 }
  
}



public static int[][] increasingMatrix(int width, int height, boolean format){
    
  if ( format == true ){
     int[][] matrix = new int[ height ][];
    for( int k = 0; k < height; k++ ){
      int index = (k * width) + 1;
      matrix[k] = new int[ width ];
        for( int i = 0; i < width; i++ ){
          matrix[k][i] = index;
          index++;
        }
    }
    return matrix;
  } else {
     int[][] matrix = new int[ width ][];
    for( int k = 0; k < width; k++ ){
      int index = k + 1;
      matrix[k] = new int[ height ];
        for( int i = 0; i < height; i++ ){
          matrix[k][i] = index;
          index += width;
        }
    }
    return matrix;
  }
  
}

public static void printMatrix(int[][] array, boolean format){
  
  if ( format == true ){
    System.out.println("row-major matrix: ");
    for( int k = 0; k < array.length; k++ ){
      for( int i = 0; i < array[k].length; i++ ){
        System.out.printf("%4d", array[k][i]);
      }
      System.out.println();
    }
  } else if ( format == false ){
    System.out.println("column-major matrix: ");
    for( int k = 0; k < array[1].length; k++ ){
      for( int i = 0; i < array.length; i++ ){
        System.out.printf("%4d", array[i][k]);
      }
      System.out.println();
    }
  }
 
}

public static int[][] translate( int[][] array ){
   int[][] trans = new int[array[1].length][array.length];
   for( int i = 0; i < trans.length; i++){
     for( int j = 0; j < trans[i].length; j++){
       trans[i][j] = array[j][i];
     }
   }
  return trans;
}
  
public static int[][] addMatrix( int[][] a, boolean formatA, int[][] b, boolean formatB){
  //initializing width and height variables for both matrices
  int widthA, heightA, widthB, heightB;
  
  //recording height and width for matrix a
  if( formatA == true){
    widthA = a[1].length;
    heightA = a.length;
  } else {
    widthA = a.length;
    heightA = a[1].length;
  }
  
  //recording height and width for matrix b
  if( formatB == true){
    widthB = b[1].length;
    heightB = b.length;
  } else {
    widthB = b.length;
    heightB = b[1].length;
  }
  
  //checking if widths and heights are equal
  if( widthA != widthB || heightA != heightB){
    System.out.println("the arrays cannot be added!");
    return null;
  } else {
  
  //checking if either array is column-major, and converting it
  if( formatA == false){
    a = translate(a);
  }else if( formatB == false){
    b = translate(b);
  }
  
  int[][] ab = new int[a.length][a[1].length];
  for( int k = 0; k < ab.length; k++ ){
    for( int i = 0; i < ab[k].length; i++ ){
      ab[k][i] = a[k][i] + b[k][i];
    }
  }
  
  return ab;
  }
}
  
  
}
