//// / / / / / / / / / / / / / ////
//// CSE 002 -  HW 01 - cch220 ////
//// / / / / / / / / / / / / / ////

public class WelcomeClass{
  
  public static void main(String args[]){
    //// Following lines print "Welcome" to the terminal window along with a signature with "cch220"
    System.out.println(" -----------");
    System.out.println("|  Welcome  |");
    System.out.println(" -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-C--C--H--2--2--0->");  
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
  }
}




  