/////////////////////////////////////     
//  Colin Hussey - CSE002 - hw 10  //
//        Prof. Brian Chen         //
/////////////////////////////////////

import java.util.Random;
import java.util.Scanner;


public class RobotCity{
public static void main ( String[] args ){
  
  //printing compass for clarity
  System.out.println("       W       ");
  System.out.println("       |       ");
  System.out.println("       |       ");
  System.out.println("S-------------N");
  System.out.println("       |       ");
  System.out.println("       |       ");
  System.out.println("       E       ");
  
  int[][] cityArray = buildCity();
  display(cityArray);
  int numRobots = (int) (Math.random()*(15));
  invade(cityArray, numRobots);
  display(cityArray);
  for(int i = 0; i < 5; i++){
    update(cityArray);
    display(cityArray);
  }

}

public static int[][] buildCity( ){
 // randomly generating EastWest dimension between 10 and 15
 int EastWest = (int) (Math.random()*((15-10)+1))+10;
    
 // randomly generating NorthSouth dimension between 10 and 15
 int NorthSouth = (int) (Math.random()*((15-10)+1))+10;
 
 // initializing cityArray with EastWest columns of length NorthSouth
 int[][] cityArray = new int[NorthSouth][EastWest];
 
 // initializing population variable
 int population;
 
 // nested for-loops iterating across entire array filling it with values
 for( int i = 0; i < NorthSouth; i++ ){
   for( int j = 0; j < EastWest; j++ ){
     // random population value between 100 and 999
     population = (int) (Math.random()*((999-100)+1))+100;
     cityArray[i][j] = population;
   }
 }
  
 return cityArray;
  
}

public static void display(int[][] a){
  // initializing block number variables to print
  int NS = a.length;
  int EW = a[1].length;
  
  // first print line indicating array to be printed
  System.out.println("Generated City Block Populations: " + EW + "(EW) x " + NS + "(NS) blocks");
  
  // nested for loop iterating across entire array and printing values
  for( int i = 0; i < a.length; i++ ){
   for( int j = 0; j < a[i].length; j++ ){
     System.out.printf("%5d", a[i][j] );
   }
    System.out.println();
  }
    
}
  
public static void invade(int[][] a, int k){  
  //initializing variables for use in for loop
  int NS = a.length;
  int EW = a[1].length;
  int EWCoord;
  int NSCoord;
  boolean overlap = true;
  
   for( int i = 0; i < k; i++){
     do{
       //randomly generating coordinates
       EWCoord = (int) (Math.random()*(EW));
       NSCoord = (int) (Math.random()*(NS));
      
       // checking if coordinate is still positive; if so, setting it to negative; if not, generating new coordinates 
       if( a[NSCoord][EWCoord] > 0){
           a[NSCoord][EWCoord] = -(a[NSCoord][EWCoord]);
           overlap = false;
       }else{ overlap = true; }
      
      }while(overlap == true);
  }
}
  
public static void update(int[][] a){
  
  for( int k = a[1].length-1; k >= 0; k--){
    if(a[a.length-1][k] < 0){
       a[a.length-1][k] = -(a[a.length-1][k]);
    }
  }
  
  for( int i = a.length-2; i >= 0; i-- ){
    for( int j = a[i].length-1; j >= 0; j-- ){
      if( a[i][j] < 0 ){
        a[i][j] = -(a[i][j]);
        a[i+1][j] = -(a[i+1][j]);
      }
    }
  }
  
}
  
  
}