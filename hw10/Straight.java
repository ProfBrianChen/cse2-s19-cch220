/////////////////////////////////////     
//  Colin Hussey - CSE002 - hw 10  //
//        Prof. Brian Chen         //
/////////////////////////////////////

import java.util.Random;
import java.util.Scanner;


public class Straight{
public static void main ( String[] args ){
  double count = 0;
  int[] deck = new int[52];
  int[] hand = new int[5];
  double numTries = 1000000;
  for( int i = 1; i < numTries; i++){
    deck = newShuffle();
    hand = newHand(deck);
    boolean straightP = straight(hand);
    if(straightP == true){
      count++;
    }
  }
  
  double percentStraight = (count/numTries)*100;
  System.out.print("Percent of hands with straight: ");
  System.out.printf("%.4f", percentStraight);
  System.out.println("%");
 
 
}

public static int[] newShuffle(){
  int[] deck = new int[52];
  int card = 0;
  for(int i = 0; i < deck.length; i++){
    deck[i] = card;
    card++;
  }
  
  for (int k = 0; k <  deck.length; k++) {
    // generating a random swap target
	  int swap = (int) (deck.length * Math.random() );
    
	  //swapping the the value of deck[i] with deck[swap]
	  int temp = deck[swap];
	  deck[swap] = deck[k];
	  deck[k] = temp;
}

  return deck;
}
  
public static int[] newHand(int[] a){
  int[] hand = new int[5];
  for( int i = 0; i < hand.length; i++){
    hand[i] = a[i];
  }
  return hand;
}
  
public static int searchLowest(int[] a, int k){
 
  //initializing new array and copying values of input array into it
  int[] ordered = new int[a.length];
  for( int x = 0; x < ordered.length; x++){
    ordered[x] = a[x];
  }
  
  //using selection sort with linear search to sort the new array
  for( int i = 0; i < ordered.length; i++ ){
    int min= 9999;
    int minIndex = i;
    for( int j = i; j < ordered.length; j++ ){
      if( ordered[j] <min ){
        min = ordered[j];
        minIndex = j;
      }
    }
    int temp = ordered[i];
    ordered[i] = ordered[minIndex];
    ordered[minIndex] = temp;
  }
  
  //returning the k-1th element of the sorted array to return the kth smallest element in the original array

  int smallest = ordered[k-1];
  return smallest;
  
}
  
public static boolean straight(int[] a){
  
  boolean straight = false;
  
  int[] c = new int[a.length];
  for( int i = 0; i < a.length; i++){
    //setting each value in c to the originals' numerical value (%13)
    c[i] = (a[i] % 13);
  }
  
 
  
  int[] b = new int[a.length];
  for( int i = 0; i < b.length; i++){
    //filling b with numerical cards from c but in order
    b[i] = searchLowest(c, (i+1));
  }
  
  // if statement to check if straight is present
  if( ((b[0] + 1) == b[1]) && 
      ((b[1] + 1) == b[2]) &&
      ((b[2] + 1) == b[3]) &&
      ((b[3] + 1) == b[4]) ){
    straight = true;
  } else{
    straight = false;
  }
  
  return straight;
  
}
  
}