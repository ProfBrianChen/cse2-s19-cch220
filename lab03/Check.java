//////////////////////////////////////
//  Colin Hussey - CSE002 - HW 03  //
//       Prof. Brian Chen           //
//////////////////////////////////////

//Importing the scanner used to read data
import java.util.Scanner;

//Creating the public class and first method
public class Check{
   public static void main(String[]args) {
 
   //Initialize scanner
   Scanner myScanner = new Scanner ( System.in );
     
   //Prompt for user input with formatting
   System.out.print("Enter the original cost of the check in the form xx.xx");
   
   //Accepting user input
   double checkCost = myScanner.nextDouble();
     
   //Propmpting for tip percentage
   System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx)");
   //Store tip percentage value in double
   double tipPercent = myScanner.nextDouble();
   //Convert input into decimal for use in calculations
   tipPercent /= 100;
   
   //Prompt user to indicate number of dinner attendees
   System.out.print("Enter the number of people who went out to dinner: ");
   //Store this value in integer
   int numPeople = myScanner.nextInt();
    
   //Initializing variables for calculations
   double totalCost;
   double costPerPerson;
   int dollars,
       dimes,
       pennies;
   //Calculating total cost of check
   totalCost = checkCost * (1+ tipPercent);
   //Calculating cost assigned to each individual
  costPerPerson = totalCost / numPeople; 
       dollars = (int)costPerPerson;
       dimes = (int) (costPerPerson * 10) % 10;
       pennies = (int) (costPerPerson * 100) % 10;
     //Printing the amount needed from each person
     System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);
     
     
    
   } //End of main/first
} //End of class