//// / / / / / / / / / / / / / ////
//// CSE 002 - Lab 01 - cch220 ////
//// / / / / / / / / / / / / / ////

public class HelloWorld{
  
  public static void main(String args[]){
    //// Following line prints "Hello, World" to the terminal window
    System.out.println("Hello, World");
    
  }
}