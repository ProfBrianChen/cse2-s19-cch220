/////////////////////////////////////     
//  Colin Hussey - CSE002 - HW 03  //
//        Prof. Brian Chen         //
/////////////////////////////////////

//Importing the scanner and a math package 
import java.util.Scanner;

public class Hw05 {
    	// main method required for every Java program
   	public static void main(String[] args) {
      
      //Initialize scanner
   Scanner myScanner = new Scanner ( System.in );
      
      //Initialize variables
      int courseNumber = 0;
      String deptName = new String("empty");
      int weekMeet = 0;
      int startHour = 0;
      int startMin = 0;
      String teachName = new String("empty");
      int numStudents = 0;
      String junkWord = new String("empty");
      
      
     while( courseNumber == 0 ){
     System.out.print( "Enter the course number: " );
     boolean a = myScanner.hasNextInt();
     if( a == true ){
       courseNumber = myScanner.nextInt();
      } else if(a != true) {
         System.out.println( "Input must be a number; try again." );
         courseNumber = 0;
         junkWord = myScanner.next();
       }
     }
    
     while( deptName.equals("empty")){
     System.out.print( "Enter the department name: " );
     boolean a = myScanner.hasNext();
     if( a == true ){
       deptName = myScanner.next();
     } else if(a != true) {
         System.out.println( "Input must be a word; try again." );
         deptName = "empty";
         junkWord = myScanner.next();
       }
     }
    
     while( weekMeet == 0 ){
     System.out.print( "Enter the number of meetings per week: " );
     boolean a = myScanner.hasNextInt();
     if( a == true ){
       weekMeet = myScanner.nextInt();
      } else if(a != true) {
         System.out.println( "Input must be a number; try again." );
         weekMeet = 0;
         junkWord = myScanner.next();
       }
     }   
      
     while( startHour == 0 ){
     System.out.print( "Enter the hour in which the class starts: " );
     boolean a = myScanner.hasNextInt();
     if( a == true ){
       startHour = myScanner.nextInt();
      } else if(a != true) {
         System.out.println( "Input must be a number; try again." );
         startHour = 0;
         junkWord = myScanner.next();
       }
     }        
      
     while( startMin == 0 ){
     System.out.print( "Enter the minute of that hour in which the class starts: " );
     boolean a = myScanner.hasNextInt();
     if( a == true ){
       startMin = myScanner.nextInt();
      } else if(a != true) {
         System.out.println( "Input must be a number; try again." );
         startMin = 0;
         junkWord = myScanner.next();
       }
     }        
      
     while( teachName.equals("empty") ){
     System.out.print( "Enter the teacher's name: " );
     boolean a = myScanner.hasNext();
     if( a == true ){
       teachName = myScanner.next();
     } else if(a != true) {
         System.out.println( "Input must be a word; try again." );
         teachName = "empty";
         junkWord = myScanner.next();
       }
     }
     
     while( numStudents == 0 ){
     System.out.print( "Enter the number of students in the class: " );
     boolean a = myScanner.hasNextInt();
     if( a == true ){
       numStudents = myScanner.nextInt();
      } else if(a != true) {
         System.out.println( "Input must be a number; try again." );
         numStudents = 0;
         junkWord = myScanner.next();
       }
     }        
    
     System.out.print ("Course " + courseNumber + " in the " + deptName + 
                       " department meets " + weekMeet + " times per week at " +
                      startHour + ":" + startMin + " and has " + numStudents + 
                      " students being taught by Mr./Mrs. " + teachName);
    }
}