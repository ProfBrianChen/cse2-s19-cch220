/////////////////////////////////////     
//  Colin Hussey - CSE002 - HW 03  //
//        Prof. Brian Chen         //
/////////////////////////////////////

//Importing the scanner and a math package 
import java.util.Scanner;

public class BoxVolume {
    	// main method required for every Java program
   	public static void main(String[] args) {
      
      //Initialize scanner
   Scanner myScanner = new Scanner ( System.in );
      
      //Prompt for user input width 
   System.out.print("The width of the box is: ");
      
      //Accepting user input
   double checkWidth = myScanner.nextDouble();
      
       //Prompt for user indput length
   System.out.print("The length of the box is: ");
      
      //Accepting user input
   double checkLength = myScanner.nextDouble();
      
      //Prompt for user indput length
   System.out.print("The height of the box is: ");
      
      //Accepting user input
   double checkHeight = myScanner.nextDouble();
      
      //Calculating volume
   double calcVolume = checkWidth * checkHeight * checkLength;
     
     //Printing vollume
   System.out.print("The volume inside the boxx is: " + calcVolume);
     
    }
}