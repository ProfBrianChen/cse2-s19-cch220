/////////////////////////////////////     
//  Colin Hussey - CSE002 - HW 03  //
//        Prof. Brian Chen         //
/////////////////////////////////////

//Importing the scanner and a math package 
import java.util.Scanner;
import java.lang.Math;

public class Convert {
    	// main method required for every Java program
   	public static void main(String[] args) {
      
      //Initialize scanner
   Scanner myScanner = new Scanner ( System.in );
      
      //Prompt for user input with formatting
   System.out.print("Enter the distance in meters: ");
   
      //Accepting user input
   double checkMeters = myScanner.nextDouble();
      
      //Converting user input to inches
   double lengthInches = checkMeters * 39.3701;
     
      //Rounding inches variable
   double finalInches = Math.floor(lengthInches * 100) / 100;
   
      //Print converted units
   System.out.print(checkMeters + " meters is " + finalInches + " inches.");
      
    }
}
     