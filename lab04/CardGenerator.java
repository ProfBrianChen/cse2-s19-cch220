//////////////////////////////////////
//  Colin Hussey - CSE002 - Lab 04  //
//       Prof. Brian Chen           //
//////////////////////////////////////

import java.util.Random;

//Creating the public class and first method
public class CardGenerator{
    public static void main(String[]args) {

    // Obtain a number between 0 and 51
    Random rand = new Random();
    int n = rand.nextInt(52);
    // Add 1 to it
    n += 1;
   
    //Declaring strings
     String cardSuit = null;
     String cardIdentity = null;
      
    //Making String for suit using if/elseif statements
    if (n <= 13){
       cardSuit = "Diamonds";
    } else if (n < 26) {
       cardSuit = "Clubs";
    } else if (n <= 39) {
       cardSuit = "Hearts";
    } else if (n <= 52) {
       cardSuit = "Spades";
  }
     //Making string variable for card identity
    if (1 < (n % 13) && (n % 13) < 11){
       cardIdentity = Integer.toString(n);
    } else if ((n % 13) == 1){
       cardIdentity = "Ace";
    } else if (n % 13 == 11){
       cardIdentity = "Jack";
    } else if (n % 13 == 12){
       cardIdentity = "Queen";
    } else if (n % 13 == 13){
       cardIdentity = "King";
    }
      
     System.out.print("You picked the " + cardIdentity + " of " + cardSuit + "\n");
  }
}