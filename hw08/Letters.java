/////////////////////////////////////     
//  Colin Hussey - CSE002 - hw 07  //
//        Prof. Brian Chen         //
/////////////////////////////////////

import java.util.Random;
import java.util.Scanner;


public class Letters {
  public static void main (String[] args){
  // main method required for every Java program
   
    // initializing length variable random between 1 and 100
    double tempVal = Math.random();
    int length = (int) (tempVal * 100);
    
    // initializing string array with random length
    String [] letters = new String[length];
    
    String letter;
    
    System.out.println();
    System.out.print("Random character array: ");
    // for loop filling array indicies with random strings
    for (int i = 0; i < letters.length; i++){
      String tempStr = randStr(); // method to get a random single letter
      letters[i] = tempStr; // assigning current index value to that letter
      System.out.print(letters[i]); // printing index
    }
    System.out.println();
    
    String [] AtoM = getAtoM(letters);
    System.out.print("AtoM characters: ");
    for (int l = 0; l < AtoM.length; l++){
      System.out.print(AtoM[l]);
    }
    System.out.println();
    
    String [] NtoZ = getNtoZ(letters);
    System.out.print("NtoZ characters: ");
    for (int l = 0; l < NtoZ.length; l++){
      System.out.print(NtoZ[l]);
    }
    System.out.println();
    System.out.println();


}
 
// method to return a random letter
public static String randStr(){
  int index; // initializing index variable

  // do-while loop to get a random number between 1 and 52
  do{
    double tempVal = Math.random();
    index = (int) (tempVal * 100);
  }while(index >= 52);
  
  // initializing 'letter' variable
  String letter = "a";
  
  // switch statement taking the random number and returning corresponding letter
  switch( index ){
    case 1: letter = "a"; break;
    case 2: letter = "b"; break;
    case 3: letter = "c"; break;
    case 4: letter = "d"; break;
    case 5: letter = "e"; break;
    case 6: letter = "f"; break;
    case 7: letter = "g"; break;
    case 8: letter = "h"; break;
    case 9: letter = "i"; break;
    case 10: letter = "j"; break;
    case 11: letter = "k"; break;
    case 12: letter = "l"; break;
    case 13: letter = "m"; break;
    case 14: letter = "n"; break;
    case 15: letter = "o"; break;
    case 16: letter = "p"; break;
    case 17: letter = "q"; break;
    case 18: letter = "r"; break;
    case 19: letter = "s"; break;
    case 20: letter = "t"; break;
    case 21: letter = "u"; break;
    case 22: letter = "v"; break;
    case 23: letter = "w"; break;
    case 24: letter = "x"; break;
    case 25: letter = "y"; break;
    case 26: letter = "z"; break;
    case 27: letter = "A"; break;
    case 28: letter = "B"; break;
    case 29: letter = "C"; break;
    case 30: letter = "D"; break;
    case 31: letter = "E"; break;
    case 32: letter = "F"; break;
    case 33: letter = "G"; break;
    case 34: letter = "H"; break;
    case 35: letter = "I"; break;
    case 36: letter = "J"; break;
    case 37: letter = "K"; break;
    case 38: letter = "L"; break;
    case 39: letter = "M"; break;
    case 40: letter = "N"; break;
    case 41: letter = "O"; break;
    case 42: letter = "P"; break;
    case 43: letter = "Q"; break;
    case 44: letter = "R"; break;
    case 45: letter = "S"; break;
    case 46: letter = "T"; break;
    case 47: letter = "U"; break;
    case 48: letter = "V"; break;
    case 49: letter = "W"; break;
    case 50: letter = "X"; break;
    case 51: letter = "Y"; break;
    case 52: letter = "Z"; break;
    default: letter = "Z"; break;
  }
  return letter; // returning letter out of method
}    
     
// getAtoM method
public static String[] getAtoM( String[] myArray ){
  String test1 = "a";
  String test2 = "m";
  String test3 = "A";
  String test4 = "M";
  int countA = 0;
  
  // for loop counting number of occurences of A - M values
  for (int k = 0; k < myArray.length; k++){
    if ( (test3.compareTo(myArray[k]) <= 0) && (test4.compareTo(myArray[k]) >= 0) ){
      countA += 1;
    }else if ( (test1.compareTo(myArray[k]) <= 0) && (test2.compareTo(myArray[k]) >= 0) ){
      countA += 1;
    }
  }
  
  // initializing array with length = number of occurences of A - M
    String [] AtoM = new String[countA];
    int index = 0;
  
  // filling new array with A - M values
  for (int j = 0; j < myArray.length; j++){
    if ( (test3.compareTo(myArray[j]) <= 0) && (test4.compareTo(myArray[j]) >= 0) ){
      AtoM[index] = myArray[j];
      index += 1;
    }else if ( (test1.compareTo(myArray[j]) <= 0) && (test2.compareTo(myArray[j]) >= 0) ){
      AtoM[index] = myArray[j];
      index += 1;
    }
  }
  
  return AtoM;
}
  
// getNtoZ method
public static String[] getNtoZ( String[] myArray ){
  String test1 = "n";
  String test2 = "z";
  String test3 = "N";
  String test4 = "Z";
  int countA = 0;
  
  for (int k = 0; k < myArray.length; k++){
    if ( (test3.compareTo(myArray[k]) <= 0) && (test4.compareTo(myArray[k]) >= 0) ){
      countA += 1;
    }else if ( (test1.compareTo(myArray[k]) <= 0) && (test2.compareTo(myArray[k]) >= 0) ){
      countA += 1;
    }
  }
  
    String [] NtoZ = new String[countA];
    int index = 0;
  
  for (int j = 0; j < myArray.length; j++){
    if ( (test3.compareTo(myArray[j]) <= 0) && (test4.compareTo(myArray[j]) >= 0) ){
      NtoZ[index] = myArray[j];
      index += 1;
    }else if ( (test1.compareTo(myArray[j]) <= 0) && (test2.compareTo(myArray[j]) >= 0) ){
      NtoZ[index] = myArray[j];
      index += 1;
    }
  }
  
  return NtoZ;
}
  
  
}
