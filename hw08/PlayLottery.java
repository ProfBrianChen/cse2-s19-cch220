/////////////////////////////////////     
//  Colin Hussey - CSE002 - hw 07  //
//        Prof. Brian Chen         //
/////////////////////////////////////

import java.util.Random;
import java.util.Scanner;


public class PlayLottery{
  public static void main ( String[] args ){
    
    int var1 = 0;
    int var2 = 0;
    int var3 = 0;
    int var4 = 0;
    
    Scanner myScanner = new Scanner ( System.in );

   // while loops to take 4 user inputs
    while( var1 == 0 ){
        System.out.print( "Input your first number: " );
        boolean a = myScanner.hasNextInt();
        if( a == true ){
            var1 = myScanner.nextInt();
          if (var1 < 0){
            var1 = 0;
            System.out.println( "Input must be a POSITIVE number; try again." );
          }
        } else if(a != true) {
            System.out.println( "Input must be a number; try again." );
            var1 = 0;
            String junkWord = myScanner.next();
      }
     }
    while( var2 == 0 ){
        System.out.print( "Input your second number: " );
        boolean a = myScanner.hasNextInt();
        if( a == true ){
            var2 = myScanner.nextInt();
          if (var2 < 0){
            var2 = 0;
            System.out.println( "Input must be a POSITIVE number; try again." );
          }
        } else if(a != true) {
            System.out.println( "Input must be a number; try again." );
            var2 = 0;
            String junkWord = myScanner.next();
      }
     }   
    while( var3 == 0 ){
        System.out.print( "Input your third number: " );
        boolean a = myScanner.hasNextInt();
        if( a == true ){
            var3 = myScanner.nextInt();
          if (var3 < 0){
            var3 = 0;
            System.out.println( "Input must be a POSITIVE number; try again." );
          }
        } else if(a != true) {
            System.out.println( "Input must be a number; try again." );
            var3 = 0;
            String junkWord = myScanner.next();
      }
     }    
    while( var4 == 0 ){
        System.out.print( "Input your fourth number: " );
        boolean a = myScanner.hasNextInt();
        if( a == true ){
            var4 = myScanner.nextInt();
          if (var4 < 0){
            var4 = 0;
            System.out.println( "Input must be a POSITIVE number; try again." );
          }
        } else if(a != true) {
            System.out.println( "Input must be a number; try again." );
            var4 = 0;
            String junkWord = myScanner.next();
      }
     }
    System.out.println("Your ticket is: " + 
                       var1 + ", " + 
                       var2 + ", " + 
                       var3 + ", " + 
                       var4);
  
  
  // initializing user input array and inputing given values
  int[] userArray = new int[4];
  userArray[0] = var1;
  userArray[1] = var2;
  userArray[2] = var3;
  userArray[3] = var4;

  int [] lottery = numbersPicked();
    
  System.out.println("The winning numbers are: " + 
                     lottery[0] + ", " + 
                     lottery[1] + ", " + 
                     lottery[2] + ", " + 
                     lottery[3]);
    
 boolean winning = userWins(userArray, lottery);
 if( winning == true ){
   System.out.println("You win!");
 } else {
   System.out.println("You lose :(");
 }
}
 
 public static boolean userWins( int[] user, int[] lottery){
   boolean userWins = false;
   if ((user[0] == lottery[0]) && 
       (user[1] == lottery[1]) && 
       (user[2] == lottery[2]) && 
       (user[3] == lottery[3])){
     userWins = true;
   }
  return userWins;
 }
 public static int[] numbersPicked(){
    //initializing lottery array
    int[] lottery = new int[4];
  
    for ( int i = 0; i < lottery.length; i++){
      //random number between 1 and 59, that isnt alreday in the array
      int num = 60;
      do{
        double tempVal1 = Math.random();
        int tempVal = (int) (tempVal1 * 100);
        tempVal = tempVal - 1;
        num = tempVal;
      }while( (num <= 59) && 
             (lottery[1] != num) && 
             (lottery[2] != num) && 
             (lottery[3] != num) );
       //putting that random number into the array
      lottery[i] = num;
    }
       
    return lottery;
  }
    
   
}