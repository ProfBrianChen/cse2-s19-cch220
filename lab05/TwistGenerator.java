//////////////////////////////////////
//  Colin Hussey - CSE002 - Lab 06  //
//       Prof. Brian Chen           //
//////////////////////////////////////

//Importing the scanner used to read data
import java.util.Scanner;

//Creating the public class and first method
public class TwistGenerator{
   public static void main(String[]args) {
 
   //Initialize scanner
   Scanner myScanner = new Scanner ( System.in );
     
   //declaring variables
   int length = 0;
   String junkWord;
   
   // prompting user for input and checking input validity
   while( length == 0 ){
     System.out.print( "Enter the desired twist length: " );
     boolean a = myScanner.hasNextInt();
     if( a == true ){
       length = myScanner.nextInt();
       if( length < 0 ){
         System.out.print( "Try again; Input MUST be a positive integer." );
         length = 0;
       }
     }
     else
        junkWord = myScanner.next();
   }  
     

// While loop with nested if statements to print the twist
//(if statements are used to save a position in order to print a pattern)
//--------------------------------------//
   int i = 0;
   while ( i < length){
     int n = 1;
     if ( n == 1 ){
        System.out.print( "\\" ); 
        n = 2;
     }
     if ( n == 2 ){
        System.out.print( " " );
        n = 3;  
     }
     if ( n == 3 ){                    
        System.out.print( "/" );
        n = 1;   
     }
 i++;                        
}//while 1
     System.out.print( "\n" );
//--------------------------------------//
    
  i = 0;
   while ( i < length){
     int n = 1;
     if ( n == 1 ){
        System.out.print( " " ); 
        n = 2;
     }
     if ( n == 2 ){
        System.out.print( "X" );
        n = 3;  
     }
     if ( n == 3 ){                    
        System.out.print( " " );
        n = 1;   
     }
 i++;                        
}//while
        System.out.print( "\n" );
//--------------------------------------//
     
    
  i = 0;
   while ( i < length){
     int n = 1;
     if ( n == 1 ){
        System.out.print( "/" ); 
        n = 2;
     }
     if ( n == 2 ){
        System.out.print( " " );
        n = 3;  
     }
     if ( n == 3 ){                    
        System.out.print( "\\" );
        n = 1;   
     }
 i++;                        
}//while
        System.out.print( "\n" );
//--------------------------------------//
   }//method
   }//class





     
     