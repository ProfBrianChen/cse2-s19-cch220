/////////////////////////////////////     
//  Colin Hussey - CSE002 - hw 07  //
//        Prof. Brian Chen         //
/////////////////////////////////////

import java.util.Random;
import java.util.Scanner;


public class arrays{
public static void main ( String[] args ){
  
  Scanner myScanner = new Scanner ( System.in );

  // asking for search type (variable 'test' -> 1=Linear, 0=Binary)
  int test = 0;
  boolean ok = false;
  while( ok == false ){
        System.out.print( "Linear(1) or Binary(0) search? input the number in (): " );
        boolean a = myScanner.hasNextInt();
        if( a == true ) {
            test = myScanner.nextInt();
          if( test == 1 || test == 0 ){
            ok = true;
          } else {
            System.out.println( "Did not enter 0 or 1, try again." );
          }
        } else if(a != true) {
            System.out.println( "Input must be a number; try again." );
            test = 0;
            String junkWord = myScanner.next();
        }
     }         
  
  // asking for array size
  int size = 0;
  while( size == 0 ){
        System.out.print( "Input the array size: " );
        boolean a = myScanner.hasNextInt();
        if( a == true ){
            size = myScanner.nextInt();
          if (size < 0){
            size = 0;
            System.out.println( "Input must be a POSITIVE number; try again." );
          }
        } else if(a != true) {
            System.out.println( "Input must be a number; try again." );
            size = 0;
            String junkWord = myScanner.next();
      }
     }
  
  // asking for search term
  int search = 0;
  while( search == 0 ){
        System.out.print( "Input the term to be searched for: " );
        boolean a = myScanner.hasNextInt();
        if( a == true ){
            search = myScanner.nextInt();
          if (search < 0){
            search = 0;
            System.out.println( "Input must be a POSITIVE number; try again." );
          }
        } else if(a != true) {
            System.out.println( "Input must be a number; try again." );
            search = 0;
            String junkWord = myScanner.next();
      }
     }

  if ( test == 1 ){
    int[] array = arrayGen( size );
    int position = LinSearch( array, search );
    for( int i = 0; i < array.length; i++ ){
      System.out.println("array index " + i + ": " + array[i]);
    }
    System.out.println("Search term position: " + position + "(-1 if search term is not present)");
  } else {
    int[] array = orderedGen( size );
    int position = BinSearch( array, search );
    for( int i = 0; i < array.length; i++ ){
      System.out.println("array index " + i + ": " + array[i]);
    }
    System.out.println("Search term position: " + position + " (-1 if search term is not present)");
  }
  
}
  
///////////////////////  a  //////////////////////////// 
public static int [] arrayGen( int Size ){
  
  int[] newArray = new int[Size];
  
 for( int i = 0; i < newArray.length; i++ ){
   
  int index = 0;
   do{
    double tempVal = Math.random();
    index = (int) (tempVal * 100);
  }while(index >= newArray.length);
  
  newArray[i] = index;
    
 }
  return newArray;
}
///////////////////////  b  //////////////////////////// 
public static int [] orderedGen( int size ){
  
  int[] ordered = new int[size];
  
  double tempVal = Math.random();
  int index = (int) (tempVal * 100);
  
  ordered[0] = index;
  
  for( int i = 1; i < ordered.length; i++ ){
    double tempVal2 = Math.random();
    int ascend = (int) (tempVal * 100);
    
    ordered[i] = ordered[i-1] + ascend;
  }
  return ordered;
}
///////////////////////  c  //////////////////////////// 
public static int LinSearch( int[] array, int term ){
  int index = 0;
  for( int i = 0; i < array.length; i++ ){
    
    if( array[i] == term ){
       index = i;
       break;
    }
    
    if( i == (array.length - 1) && array[i] != term ){
      return -1;
    }
    
  }
  return index;
}
///////////////////////  d  //////////////////////////// 
public static int BinSearch( int[] array, int term ){
    int start = 0;
    int middle = 0;
    int end = array.length - 1;

    while( start <= end ) {
      middle = ( start + end ) / 2;
      if ( array[middle] < term){
        start = middle + 1;
      } else if ( array[middle] > term ){
        end = middle - 1;
      } else {
        return middle;
      }
    }
  return -1;
  }
/////////////////////////////////////////////////////
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
}