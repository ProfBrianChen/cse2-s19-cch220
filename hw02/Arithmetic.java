////////////////////////////////////
// Colin Hussey - CSE 002 - HW 02 //
//       Prof. Brian Chen         //
////////////////////////////////////


public class Arithmetic {
    	// main method required for every Java program
   	public static void main(String[] args) {

    //pairs of pants
    int numPants = 3;
    //Cost per pair
    double pantsPrice = 34.98;
     
    //# of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;
      
    //Number of belts
    int numBelts = 1;
    //Cost per belts
    double beltPrice = 33.99;
      
    //Tax rate
    double paSalesTax = 0.06;
      
    //Total Cost of each item
    double totalPantsPrice = numPants * pantsPrice;
    double totalShirtsPrice = numShirts * shirtPrice;
    double totalBeltsPrice = numBelts * beltPrice;
      
    //Tax on total of each item
    double pantsTax = totalPantsPrice * paSalesTax;
    double shirtsTax = totalShirtsPrice * paSalesTax;
    double beltsTax = totalBeltsPrice * paSalesTax;
     
    //Total cost of purchases (before tax)
    double totalPrice = totalBeltsPrice + totalShirtsPrice + totalPantsPrice;
      
    //Total sales tax
    double totalTax = totalPrice * paSalesTax;
      
    //Total due
    double totalDue = totalPrice + totalTax;
    
    //Total Paid (rounded)
    int totalRounded1 = (int)(totalDue * 100);
    double totalRounded = totalRounded1 / 100.0;
      
      
    System.out.println("Total amount due is " + totalRounded);

      
	}  //end of main method   
} //end of class
