  /////////////////////////////////////     
//  Colin Hussey - CSE002 - lab 08 //
//        Prof. Brian Chen         //
/////////////////////////////////////

import java.util.Random;
import java.util.Scanner;
import java.util.Arrays;
import java.lang.Math;


public class ArraysLab{
public static void main(String[] args){

// Step 1. Declaring length variable and int array with that length
  int length;
  do{
    double tempVal = Math.random();
    length = (int) (tempVal * 100);
  }while(length < 50);
  
  int [] array = new int[length];
  System.out.println("Array length: " + length);

  
// Step 2. filling array with random integers
  for( int i = 0; i < array.length; i++){
    double tempVal2 = Math.random() * 100;
    array[i] = (int) tempVal2;
    System.out.println("Array value " + i + " = " + array[i]);
  }
  
  double mean;
  System.out.println("---------------------------------------------");
  getRange(array);
  mean = getMean(array);
  getStdDev(array, mean);
  System.out.println("---------------------------------------------");
  shuffle(array);
}
  
  
// Step 3. Creating method to get the range of values in the array
  public static int getRange(int[] array){
    Arrays.sort(array);
    int range = array[array.length-1] - array[1];
    System.out.println("Range of values in the array is: " + range);
    return range;
  }
 
  
// Step 4. Creating method to get the mean of values in the array
  public static double getMean(int[] array){
    double mean;
    int total = 0;
    for (int index = 0; index < array.length; index++){
      total += array[index];
    }
    mean = total / array.length;
    System.out.println("Mean of values in the array is: " + mean);
    return mean;
  }
 
  
// Step 5. Creating method to calculate the standard deviation of values in the array
  public static double getStdDev(int[] array, double mean){
    double diff;
    double diffSq;
    double SoS = 0;
    int divisor = array.length - 1;
    double StdDev;
    for (int index = 0; index < array.length; index++){
      diff = array[index] - mean;
      diffSq = Math.pow(diff, 2);
      SoS += diffSq;
    }
    double tempVal = SoS / divisor;
    StdDev = Math.pow(tempVal, 0.5);
    System.out.println("The standard deviation among values in the array is: " + StdDev);
    return StdDev;
  }
  
// Step 6. Creating method to shuffle the array
  public static void shuffle(int[] array){
    for (int index = 0; index < array.length; index++){
      int target = (int) (array.length * Math.random() );
      int temp = array[target];
      array[target] = array[index];
      array[index] = temp;
    }
    for (int index = 0; index < array.length; index++){
      System.out.println("Shuffled array value " + index + " = " + array[index]);
    }
   
  }
  
  
  
}
